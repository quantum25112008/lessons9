package dto;

public class Human {
    String name;
    String lastname;
    String surname;
    int weight;
    int age;
    String gender;

//    public Human(String name, String lastname, String surname, int weight, int age, String gender) {
//        this.name = name;
//        this.lastname = lastname;
//        this.surname = surname;
//        this.weight = weight;
//        this.age = age;
//        this.gender = gender;
//    }

    public Human(){

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", surname='" + surname + '\'' +
                ", weight=" + weight +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}
