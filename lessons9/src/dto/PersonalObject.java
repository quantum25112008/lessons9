package dto;

public class PersonalObject {
    String model;
    String color;
    int wheels;
    int steeringwheel;
    int doors;
    String airbag;
    String headlights;


    public PersonalObject(String model, String color, int wheels, int steeringwheel, int doors, String airbag, String headlights) {
        this.model = model;
        this.color = color;
        this.wheels = wheels;
        this.steeringwheel = steeringwheel;
        this.doors = doors;
        this.airbag = airbag;
        this.headlights = headlights;
    }

   public PersonalObject(){

   }
    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public void setSteeringwheel(int steeringwheel) {
        this.steeringwheel = steeringwheel;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public void setAirbag(String airbag) {
        this.airbag = airbag;
    }

    public void setHeadlights(String headlights) {
        this.headlights = headlights;
    }

    @Override
    public String toString() {
        return "PersonalObject{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", wheels=" + wheels +
                ", steeringwheel=" + steeringwheel +
                ", doors=" + doors +
                ", airbag='" + airbag + '\'' +
                ", headlights='" + headlights + '\'' +
                '}';
    }
}
