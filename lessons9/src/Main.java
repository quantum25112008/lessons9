import dto.PersonalObject;
import dto.Human;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите своё имя:");
        String name = scanner.readLine();
        System.out.println("Введите свою фамилию:");
        String lastname = scanner.readLine();
        System.out.println("Введите своё отчество:");
        String surname = scanner.readLine();
        System.out.println("Введите свой возраст:");
        int age = Integer.parseInt(scanner.readLine());
        System.out.println("Введите свой вес:");
        int weight = Integer.parseInt(scanner.readLine());
        System.out.println("Введите свой пол:");
        String gender = scanner.readLine();

        Human human = new Human();
        human.setName(name);
        human.setLastname(lastname);
        human.setSurname(surname);
        human.setAge(age);
        human.setWeight(weight);
        human.setGender(gender);

        System.out.println(human.toString());
    }
}


















