package dto;

public class Order {
    public static final String SUCCES = "SUCCES";

    public  static final String FAILED = "FAILED";

    public Order(Integer orderId, Long amount, String status) {
        this.orderId = orderId;
        this.amount = amount;
        this.status = status;
    }

    Integer orderId;
    Long amount;
    String status;


}
